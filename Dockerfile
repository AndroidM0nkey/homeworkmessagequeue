FROM python:3.7

RUN mkdir /root/app/
WORKDIR /root/app/

COPY . .
RUN pip install -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/root/app/"
EXPOSE 8000

CMD ["bash", "-c", "while ! curl -s rabbitmq:15672 > /dev/null; do echo waiting for rabbitmq; sleep 3; done; python3 server.py"]