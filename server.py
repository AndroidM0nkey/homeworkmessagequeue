import pika, sys, os
import requests
from bs4 import BeautifulSoup
from collections import deque
import sys

from concurrent import futures
import logging

import grpc
import protocol_pb2
import protocol_pb2_grpc


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        body = str(body)
        s = body[2:body.rfind("|")]
        t = str(body[body.rfind("|") + 1:])
        q = deque()
        print(s,t)
        used = set()
        p = {}
        q.append(s)
        search = True
        while(search):
            cur = q.popleft()
            response = requests.get(
                url=cur
            )

            soup = BeautifulSoup(response.content, 'html.parser')
            allLinks = soup.find(id="bodyContent").find_all("a")

            for link in allLinks:
                try:
                    if link['href'].find("/wiki/") != -1 and link['href'].find("/wiki/File:") == -1:
                        nlink = "https://en.wikipedia.org" + link['href'] 
                        if nlink not in used:
                            used.add(nlink)
                            q.append(nlink)
                            p[nlink] = cur
                        if nlink == t:
                            search = False
                            break
                except:
                    continue
        path = [t]
        cur = t
        while(cur != s):
            cur = p[cur]
            path.append(cur)
        original_stdout = sys.stdout
        with open('putput.txt', 'w') as f:
            sys.stdout = f
            print(path)
            sys.stdout = original_stdout

    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)
    channel.start_consuming()

def worker():
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

class Greeter(protocol_pb2_grpc.GreeterServicer):
        def SayHello(self, request, context):
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host='localhost'))
            channel = connection.channel()

            channel.queue_declare(queue='hello')
            s = request.s
            t = request.t

            msg  = s + '|' + t
            channel.basic_publish(exchange='', routing_key='hello', body=msg)
            print("successfully send!'")
            connection.close()

            sys.sleep(10)#slow server/heavy task
            f = open("output.txt", "r")
            return protocol_pb2.HelloReply(message=f.read())

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    protocol_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
    server.add_insecure_port('[::]:8080')
    server.start()
    server.wait_for_termination()

print('Server starting')
import threading
th = threading.Thread(target=serve)
print('Worker starting')
worker()