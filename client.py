from __future__ import print_function

import logging

import grpc
import protocol_pb2
import protocol_pb2_grpc

ip = 'localhost:8000'
s = 'https://en.wikipedia.org/wiki/Rudolf_Bayer'
t = 'https://en.wikipedia.org/wiki/Red%E2%80%93black_tree'

def run():
    with grpc.insecure_channel(ip) as channel:
        stub = protocol_pb2_grpc.GreeterStub(channel)
        response = stub.SayHello(protocol_pb2.HelloRequest(s = s, t = t))
    print("Server replied: " + response.message)

if __name__ == '__main__':
    print('enter first url:')
    s = input()
    print('enter second url')
    t = input()

    run()